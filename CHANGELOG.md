# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.0"></a>
# 0.1.0 (2018-11-11)


### Features

* First try for static html file to heruko ([c9f3543](https://gitlab.com/DomoticzEnergyVisualiser/domevisualiser/commit/c9f3543))
